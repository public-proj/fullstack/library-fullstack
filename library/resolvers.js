const Author = require('./models/author');
const Book = require('./models/book');
const User = require('./models/user');
const jwt = require('jsonwebtoken');

const { PubSub } = require('graphql-subscriptions');
const author = require('./models/author');
const pubsub = new PubSub();

const resolvers = {
  Query: {
    bookCount: () => Book.collection.countDocuments(),
    authorCount: () => Author.collection.countDocuments(),
    allBooks: async (root, args) => {
      if (args.author && args.genre) {
        const authorId = await Author.findOne({ name: args.author }).select(
          '_id'
        );
        return await Book.find({
          author: authorId,
          genres: { $in: args.genre },
        }).populate('author');
      }
      if (args.author) {
        const authorId = await Author.findOne({ name: args.author }).select(
          '_id'
        );
        return Book.find({ author: authorId }).populate('author');
      }
      if (args.genre) {
        return await Book.find({ genres: { $in: args.genre } }).populate(
          'author'
        );
      }
      return await Book.find({}).populate('author');
    },
    allAuthors: async () => {
      const authors = await Author.find({}).lean();
      authors.map((author) => (author.bookCount = author.booksWritten.length));

      return authors;

      /* allAuthors: async () => {
      const authors = await Author.find({}).lean();

      const authorsWithBookCounts = await Promise.all(
        authors.map(async (author) => {
          const count = await Book.find({
            author: author._id,
          }).countDocuments();
          return {
            ...author,
            bookCount: count,
          };
        })
      );
      return authorsWithBookCounts; */
    },
    me: (_, __, context) => {
      return context.currentUser;
    },
  },
  Mutation: {
    addBook: async (_, { title, author, published, genres }, context) => {
      const currentUser = context.currentUser;
      /* TODO: uncomment this
      if (!currentUser) {
        throw new AuthenticationError('not authenticated');
      }
      */
      try {
        let foundAuthor = await Author.findOne({ name: author });

        if (foundAuthor === null) {
          const newAuthor = new Author({ name: author });
          const savedAuthor = await newAuthor.save();
          foundAuthor = savedAuthor;
        }

        const book = new Book({
          title,
          published,
          genres,
          author: foundAuthor.id,
        });
        const savedBook = await book.save();

        await foundAuthor.updateOne({
          $push: { booksWritten: savedBook._id },
        });

        pubsub.publish('BOOK_ADDED', {
          bookAdded: savedBook.populate('author'),
        });
        return savedBook.populate('author');
      } catch (error) {
        throw new Error(error.message);
      }
    },
    editAuthor: async (_, { name, setBornTo }, context) => {
      const currentUser = context.currentUser;

      if (!currentUser) {
        throw new AuthenticationError('not authenticated');
      }

      const author = await Author.findOne({ name });
      if (!author) {
        throw new UserInputError('Author does not exist');
      }
      try {
        author.born = setBornTo;
        await author.save();
      } catch (error) {
        throw new UserInputError(error.message);
      }
      return author;
    },
    createUser: async (root, args) => {
      const user = new User({
        username: args.username,
        password: args.password,
        favouriteGenre: args.favouriteGenre,
      });
      try {
        const savedUser = user.save();
        return savedUser;
      } catch (error) {
        throw new UserInputError(error.message, { invalidArgs: args });
      }
    },
    login: async (root, args) => {
      const user = await User.findOne({ username: args.username });
      if (!user || args.password !== 'secret') {
        throw new UserInputError('wrong credentials');
      }

      const userForToken = {
        username: user.username,
        id: user._id,
      };

      return { value: jwt.sign(userForToken, process.env.JWT_SECRET) };
    },
  },
  Subscription: {
    bookAdded: {
      subscribe: () => pubsub.asyncIterator(['BOOK_ADDED']),
    },
  },
};

module.exports = resolvers;
