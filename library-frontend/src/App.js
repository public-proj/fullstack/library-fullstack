import React, { useState } from 'react';
import { useApolloClient, useQuery, useSubscription } from '@apollo/client';
import { ALL_AUTHORS, ALL_BOOKS, BOOK_ADDED } from './queries';
import Authors from './components/Authors';
import Books from './components/Books';
import NewBook from './components/NewBook';
import LoginForm from './components/LoginForm';
import Recommend from './components/Recommend';

const App = () => {
  //eslint-disable-next-line
  const [token, setToken] = useState(null);
  const [page, setPage] = useState('authors');
  const [error, setError] = useState(null);
  const client = useApolloClient();

  const authorResults = useQuery(ALL_AUTHORS);
  const bookResults = useQuery(ALL_BOOKS, { variables: { genre: null } });

  const updateCacheWith = (addedBook) => {
    const genresToUpdate = addedBook.genres.concat(null);

    const includedIn = (set, object) => {
      set.map((p) => p.id).includes(object.id);
    };

    genresToUpdate.forEach((genre) => {
      const dataInStore = client.readQuery({
        query: ALL_BOOKS,
        variables: { genre },
      });

      if (dataInStore && !includedIn(dataInStore.allBooks, addedBook)) {
        client.writeQuery({
          query: ALL_BOOKS,
          variables: { genre },
          data: {
            ...dataInStore,
            allBooks: dataInStore?.allBooks.concat(addedBook),
          },
        });
      }
    });
  };

  useSubscription(BOOK_ADDED, {
    onSubscriptionData: ({ subscriptionData }) => {
      updateCacheWith(subscriptionData.data.bookAdded);
    },
  });

  if (authorResults.loading || bookResults.loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="container">
      <div>
        <button onClick={() => setPage('authors')}>authors</button>
        <button onClick={() => setPage('books')}>books</button>
        {token ? (
          <>
            <button onClick={() => setPage('add')}>add book</button>
            <button onClick={() => setPage('recommend')}>recommend</button>
            <button
              onClick={() => {
                setToken(null);
                localStorage.removeItem('library-user-token');
                client.resetStore();
              }}
            >
              Logout
            </button>
          </>
        ) : (
          <button onClick={() => setPage('login')}>login</button>
        )}
      </div>
      <div style={{ visibility: error === null ? 'hidden' : 'visible' }}>
        {error}
        <button onClick={() => setError(null)}>x</button>
      </div>
      <Authors
        show={page === 'authors'}
        setError={setError}
        authors={authorResults?.data?.allAuthors}
        refetch={authorResults.refetch}
      />

      <Books
        show={page === 'books'}
        setError={setError}
        books={bookResults?.data?.allBooks}
      />

      <NewBook
        setError={setError}
        show={page === 'add'}
        updateCacheWith={updateCacheWith}
      />

      <Recommend
        show={page === 'recommend'}
        books={bookResults?.data?.allBooks}
      />

      <LoginForm
        show={page === 'login'}
        setToken={setToken}
        setError={setError}
        setPage={setPage}
      />
    </div>
  );
};

export default App;
