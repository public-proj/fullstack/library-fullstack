import { useQuery } from '@apollo/client';
import React from 'react';
import { ME } from '../queries';

const Recommend = (props) => {
  const queryMeResults = useQuery(ME);

  if (!props.show) {
    return null;
  }

  if (queryMeResults.loading) {
    return <div>Loading...</div>;
  }
  const recommendedBooks = props.books.filter((book) =>
    book.genres.includes(queryMeResults.data.me.favouriteGenre)
  );
  return (
    <div>
      <h2>recommendations</h2>
      <table className="table">
        <tbody>
          <tr>
            <th></th>
            <th>author</th>
            <th>published</th>
          </tr>
          {recommendedBooks?.map((a) => (
            <tr key={a.title}>
              <td>{a.title}</td>
              <td>{a.author.name}</td>
              <td>{a.published}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Recommend;
